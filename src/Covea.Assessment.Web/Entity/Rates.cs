﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Covea.Assessment.Web.Entity
{
	public class Rates
	{
		public decimal SumAssured { get; set; }
		public decimal Rate { get; set; }
	}
}
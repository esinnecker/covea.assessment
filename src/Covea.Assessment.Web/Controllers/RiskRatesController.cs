﻿using Covea.Assessment.Domain.Entities;
using Covea.Assessment.Infra.Data.ORM.EF;
using Covea.Assessment.Web.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Covea.Assessment.Web.Controllers
{
    public class RiskRatesController : Controller
    {
        private AssessmentDbContext db = new AssessmentDbContext();       
        

        // GET: RiskRates
        public ActionResult Index()
        {            
            return View("Index", "_Layout", db.RiskRates.ToList());
        }

		[HttpPost]
		public ActionResult Calculate(decimal pSumAssured, int pAge)
		{
            decimal _grossPremium = 0.0m;
            if (ModelState.IsValid)
            {
                List<RiskRates> listRiskRates = new List<RiskRates>();
                listRiskRates = db.RiskRates.ToList();

                decimal _sumAssuredFormated = decimal.Parse(pSumAssured.ToString("F3", CultureInfo.InvariantCulture));

                string _messageOutput;
                Rates _ratesOutput;
                if (!IsSumAssuredAgeValid(_sumAssuredFormated, pAge, listRiskRates, out _messageOutput, out _ratesOutput))
                {
                    return Json(new { success = false, message = _messageOutput }, JsonRequestBehavior.AllowGet);
                }
				else
				{
                    _grossPremium = CalculateMounthlyPremium(_ratesOutput);                    
                }
            }
            return View();
        }

        public bool IsSumAssuredAgeValid(decimal pSumAssured, int pAge, List<RiskRates> pListRiskRates, out string pMessageOutput, out Rates pRates)
		{
            decimal _sumAssured;
            decimal _rate;
            Rates _rates = new Rates();
            if (ModelState.IsValid)
			{                
                if (pAge > 31 && pAge <= 50)
				{
                    _sumAssured = pListRiskRates.Where(list => list.AgeBetween31and50!=null).Max(l => l.SumAssuredValue);                    
                    if (pSumAssured > _sumAssured)
					{
                        pMessageOutput = "The maximum sum assured for 31-50 years old is £" + _sumAssured.ToString();
                        pRates = null;
                        return false;
                    }
                    _rate = (decimal)pListRiskRates.Where(list => list.SumAssuredValue.Equals(pSumAssured)).First().AgeBetween31and50;

                }
                else if (pAge > 50)
                {
                    _sumAssured = pListRiskRates.Where(list => list.AgeMore50 != null).Max(l => l.SumAssuredValue);                    
                    if (pSumAssured > _sumAssured)
                    {
                        pMessageOutput = "The maximum sum assured for more 51 years old is £" + _sumAssured.ToString();
                        pRates = null;
                        return false;
                    }
                    _rate = (decimal)pListRiskRates.Where(list => list.SumAssuredValue.Equals(pSumAssured)).First().AgeMore50;
                }
                else
				{
                    _sumAssured = pSumAssured;
                    _rate = (decimal)pListRiskRates.Where(list => list.SumAssuredValue.Equals(pSumAssured)).First().AgeLess30;
                }
                _rates = new Rates
                {
                    SumAssured = _sumAssured,
                    Rate = _rate
                };
            }
            pRates = _rates;
            pMessageOutput = null;
			return true;
		}

        public decimal CalculateMounthlyPremium(Rates pRates)
		{
            decimal _riskPremium = 0.0m;
            decimal _renewalComission = 0.0m;
            decimal _netPremium = 0.0m;
            decimal _initialComission = 0.0m;
            decimal _grossPremium = 0.0m ;

            if (ModelState.IsValid)
			{
				_riskPremium = pRates.Rate * (pRates.SumAssured / 1000);
                _renewalComission = 0.03m * _riskPremium;
                _netPremium = _riskPremium + _renewalComission;
                _initialComission = _netPremium * 2.05m;
                _grossPremium = _netPremium * _initialComission;
            }
			
			return _grossPremium;
		}
	}
}
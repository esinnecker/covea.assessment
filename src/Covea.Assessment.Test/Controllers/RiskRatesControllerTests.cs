﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Covea.Assessment.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Covea.Assessment.Web;
using Covea.Assessment.Domain.Entities;
using System.Globalization;
using Covea.Assessment.Infra.Data.ORM.EF;
using Covea.Assessment.Web.Entity;

namespace Covea.Assessment.Web.Controllers.Tests
{
	[TestClass()]
	public class RiskRatesControllerTests
	{
		[TestMethod()]
		public void CalculateMounthlyPremium_Age18_Success()
		{
			RiskRatesController _riskRatesController = new RiskRatesController();
			Rates _rates = new Rates();

			_rates.SumAssured = 25.000m;
			_rates.Rate = 18;

			Assert.AreEqual(0.440406112500m, _riskRatesController.CalculateMounthlyPremium(_rates));
		}

		[TestMethod()]
		public void CalculateMounthlyPremium_Age18_Fail()
		{
			RiskRatesController _riskRatesController = new RiskRatesController();
			Rates _rates = new Rates();

			_rates.SumAssured = 25.000m;
			_rates.Rate = 18;

			Assert.AreEqual(0.440000000000m, _riskRatesController.CalculateMounthlyPremium(_rates));
		}

		[TestMethod()]
		public void CalculateMounthlyPremium_Age30_Success()
		{
			RiskRatesController _riskRatesController = new RiskRatesController();
			Rates _rates = new Rates();

			_rates.SumAssured = 50.000m;
			_rates.Rate = 30;

			Assert.AreEqual(4.893401250000m, _riskRatesController.CalculateMounthlyPremium(_rates));
		}

		[TestMethod()]
		public void CalculateMounthlyPremium_Age30_Fail()
		{
			RiskRatesController _riskRatesController = new RiskRatesController();
			Rates _rates = new Rates();

			_rates.SumAssured = 25.000m;
			_rates.Rate = 18;

			Assert.AreEqual(4.000000000000m, _riskRatesController.CalculateMounthlyPremium(_rates));
		}
	}
}
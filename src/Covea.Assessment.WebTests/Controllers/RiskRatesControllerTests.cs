﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Covea.Assessment.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Covea.Assessment.Web.Controllers.Tests
{
	[TestClass()]
	public class RiskRatesControllerTests
	{
		[TestMethod()]
		public void CalculateTest()
		{
			throw new NotImplementedException();
		}

		public void CalculateTest_Age18_Success()
		{
			RiskRatesController riskRatesController = new RiskRatesController();

			decimal _sumAssured = 25m;
			int _age = 18;

			Assert.AreEqual(0.00000040212884050000, riskRatesController.Calculate(_sumAssured, _age));
		}
	}
}
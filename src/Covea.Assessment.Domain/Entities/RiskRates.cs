﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Covea.Assessment.Domain.Entities
{
	public class RiskRates
	{
		[Key]
		public int Id { get; set; }
		
		[DisplayFormat(DataFormatString = "{0:F3}")]
		public decimal SumAssuredValue { get; set; }

		[DisplayFormat(DataFormatString = "{0:F4}")]		
		public decimal? AgeLess30 { get; set; }

		[DisplayFormat(DataFormatString = "{0:F4}")]		
		public decimal? AgeBetween31and50 { get; set; }

		[DisplayFormat(DataFormatString = "{0:F4}")]
		public decimal? AgeMore50 { get; set; }
	}
}

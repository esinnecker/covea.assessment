﻿using Covea.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Covea.Assessment.Infra.Data.ORM.EF
{
	public class AssessmentDbContext : DbContext
	{
		public AssessmentDbContext() : base("connCoveaDbContext")
		{
		}

		public DbSet<RiskRates> RiskRates { get; set; }
		
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			//Removendo convencoes
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
			//modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
			//modelBuilder.Conventions.Add(new DecimalPropertyConvention(38, 18));

			modelBuilder.Properties()
				.Where(p => p.Name == p.ReflectedType.Name + "Id")
				.Configure(p => p.IsKey());

			modelBuilder.Properties<string>()
				.Configure(p => p.HasColumnType("varchar"));

			modelBuilder.Properties<string>()
				.Configure(p => p.HasMaxLength(80));

			modelBuilder.Entity<RiskRates>().Property(m => m.SumAssuredValue).HasPrecision(6, 3);
			modelBuilder.Entity<RiskRates>().Property(m => m.AgeLess30).HasPrecision(5, 4);
			modelBuilder.Entity<RiskRates>().Property(m => m.AgeBetween31and50).HasPrecision(5, 4);
			modelBuilder.Entity<RiskRates>().Property(m => m.AgeMore50).HasPrecision(5, 4);

			base.OnModelCreating(modelBuilder);
		}
	}
}

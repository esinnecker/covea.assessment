namespace Covea.Assessment.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialIdentityMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RiskRates", "AgeMore50", c => c.Decimal(precision: 5, scale: 4));
            DropColumn("dbo.RiskRates", "AgeMore51");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RiskRates", "AgeMore51", c => c.Decimal(precision: 5, scale: 4));
            DropColumn("dbo.RiskRates", "AgeMore50");
        }
    }
}

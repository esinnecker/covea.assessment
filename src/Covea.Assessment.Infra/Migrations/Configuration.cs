namespace Covea.Assessment.Infra.Migrations
{
	using Covea.Assessment.Domain.Entities;
	using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Covea.Assessment.Infra.Data.ORM.EF.AssessmentDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Covea.Assessment.Infra.Data.ORM.EF.AssessmentDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.RiskRates.AddOrUpdate(x => x.Id,
                new RiskRates() { Id = 1, SumAssuredValue = 25.000M, AgeLess30 = 0.0172M, AgeBetween31and50 = 0.1043M, AgeMore50 = 0.2677M },
                new RiskRates() { Id = 2, SumAssuredValue = 50.000M, AgeLess30 = 0.0165M, AgeBetween31and50 = 0.0999M, AgeMore50 = 0.2565M },
                new RiskRates() { Id = 3, SumAssuredValue = 100.000M, AgeLess30 = 0.0154M, AgeBetween31and50 = 0.0932M, AgeMore50 = 0.2393M },
                new RiskRates() { Id = 4, SumAssuredValue = 200.000M, AgeLess30 = 0.0147M, AgeBetween31and50 = 0.0887M, AgeMore50 = 0.2285M },
                new RiskRates() { Id = 5, SumAssuredValue = 300.000M, AgeLess30 = 0.0144M, AgeBetween31and50 = 0.0872M, AgeMore50 = null },
                new RiskRates() { Id = 6, SumAssuredValue = 500.000M, AgeLess30 = 0.0146M, AgeBetween31and50 = null, AgeMore50 = null }
            );
        }
    }
}
